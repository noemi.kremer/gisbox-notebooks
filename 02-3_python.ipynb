{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Python programming: Functions, Classes, Methods\n",
    "## Functions\n",
    "\n",
    "Functions allow us to bundle several instructions into separate units that then can be called from other parts of the program.\n",
    "\n",
    "![python-function](./img/python-function.png)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def printHello():\n",
    "    print (\"Hello from within a function\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Subsequently, the function defined in this way can be called in the script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "printHello()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us start with an example. What we have up here is our first function. You need the **def** declaration, as it tells Python that you are just about to create a function. Followed by the function name, here **printHello**. The brackets are empty here, but if one or more arguments are passed, that is the place to put them in. To call the function just use name and brackets, that is it.\n",
    "\n",
    "Functions are often used to make more complex computations and return a value. This is accomplished by the **return** keyword. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def three():\n",
    "    return 3\n",
    "\n",
    "result = 3 + three()\n",
    "print (result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arguments\n",
    "\n",
    "We can hand over values to functions using so-called __arguments__ (sometimes also called function parameters) that are then available within the scope of the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plusThree(number):\n",
    "    print (f\"input: {number}\")\n",
    "    return number + 3\n",
    "\n",
    "result = plusThree(5)\n",
    "print (f\"the result is {result}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiple arguments are separated by commas and handled in the sequence they have been handed over to the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def subtract (first, second):\n",
    "    return first - second\n",
    "subtract(4,5)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subtract(5,4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Default argument values\n",
    "Function argument can be defined with default values, that are used as a fallback, when the argument is not provided. This is especially useful, when many arguments are available to configure specific aspects only in special cases. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add(firstValue=3, secondValue=5):\n",
    "    return firstValue + secondValue\n",
    "\n",
    "print (add(8,4))\n",
    "print (add(6))\n",
    "print (add())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Argument with keyword names\n",
    "In the **add** function in the cell above not only a default values was provided, but the arguments have got names too. This allows to hand over the arguments in an arbitrary order in the form **argumnent name = value** when calling the function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "add(secondValue = 3, firstValue = 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Arbitrary number of arguments: `*args`\n",
    "\n",
    "Functions can be passed any number of parameters. For this purpose, the *unpack operator* `*` is prefixed to the argument variable in the definition of the function. The arguments are then treated as tuples that can be processed with a loop, for example. \n",
    "\n",
    "As with all other functions, the argument can be freely chosen, but the name `*args` has become the convention here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def addAll(*numbers):\n",
    "    print(f\"The function was started with {len(numbers)} Arguments called: {numbers}\" )\n",
    "    result = 0\n",
    "    for number in numbers:\n",
    "        result = result + number\n",
    "    return result\n",
    "\n",
    "addAll(1,3,4,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addAll(4,5.6,0.440,5,0.11,-0.33)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Any number of named arguments: `**kwargs` \n",
    "Similar to tuples of `*args`, arguments named with a preceding `**` can be passed __k__ey __w__ord __arg__uments, which by convention are often passed with `**kwargs`. The arguments are a dictionary, and can be processed individually or in a loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def printcolours(**kwargs):\n",
    "    print (f\"The following arguments were put forward {kwargs}\")\n",
    "    # Instead of kwargs['blue'] we use the get(value, default value) method,\n",
    "    # If no value \"blue\" was transferred \n",
    "    print (f\"blue : {kwargs.get('blue',0)}\")\n",
    "    for keys, values in kwargs.items():\n",
    "        print (f\"Key: {keys} Value: {values}\")\n",
    "\n",
    "printcolours(blue=1, rot = 0, green= 0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "printcolours (brown=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Classes\n",
    "A class is a template from which objects can be instantiated. All objects, i.e. instances of a class, have the same properties.\n",
    "\n",
    "![Python-class](./img/python-class.png)  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Material:\n",
    "    name = \"without name\"\n",
    "    density = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The instance of a class as an object is created with the so-called constructor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "wood = Material()\n",
    "print (wood.name)\n",
    "#dem material einen Namen geben\n",
    "wood.name = \"Beech\"\n",
    "print (wood.name)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dir(wood)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Methods\n",
    "Functions defined within a class are called __methods__ and can be called with the pattern **object instance.method()**. \n",
    "Methods always have a first parameter **self** which is automatically filled by the interpreter during the call and does not have to be specified by the programmers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Material:\n",
    "    name = \"without name\"\n",
    "    density = 0    \n",
    "    def getVolumeWeight(self, volume_in_m3):\n",
    "        return volume_in_m3 * self.density\n",
    "\n",
    "beech = Material()\n",
    "beech.name = \"beech\"\n",
    "beech.density = 720\n",
    "beech.getVolumeWeight(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All classes have automatically created standard methods that are defined during the class definition itself. Among the very common methods is the **__init__** method, the so-called constructor, which is executed when a new object is created and sets initial properties."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inheritance "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Animal:\n",
    "    def greet(self):\n",
    "        print (f\"{self.name} greets silently\")\n",
    "    \n",
    "    def __init__(self, name=\"Anonymus\"):\n",
    "        self.name = name\n",
    "   \n",
    "Waldi = Animal(\"Waldi\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "Waldi.greet()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Hasso = Animal()\n",
    "Lassie = Animal()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Lassie.name = \"Lassie\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Lassie.greet()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog(Animal):\n",
    "    def greet(self):\n",
    "        print(f\"{self.name} bellt!\")\n",
    "\n",
    "class Cat(Animal):\n",
    "    def greet(self):\n",
    "        print(f\"{self.name} miaut!\")\n",
    "\n",
    "Bello = Dog()\n",
    "Mietz = Cat()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mietz.greet()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(Mietz)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algorithm\n",
    "After introducing Python programming basics, let us quickly talk about algorithms. Since you are able now to actually write your own code, you are ready to create an algorithm using Python programming. \n",
    "\n",
    "Well, I am pretty sure you have had contact with algorithms already.\n",
    "\n",
    "An algorithm is a unique set of rules for solving a problem. It does not necessarily have to be code in programming languages, an algorithm can also be written in natural language or, for example, pseudocode. There are a few important properties that make up an algorithm, including determinism, finiteness, and effectiveness. For example, a calculation formula such as the Pythagorean theorem is an algorithm.\n",
    "<div>\n",
    "<img src=\"./img/algorithm.png\" width=\"1000\"/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
